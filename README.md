Website
=======
https://www.xiph.org/

License
=======
BSD-style license (see the file source/COPYING)

Version
=======
1.3.6

Source
======
libvorbis-1.3.6.tar.xz (sha256: af00bb5a784e7c9e69f56823de4637c350643deedaf333d0fa86ecdba6fcb415)

Requires
========
* Ogg

Required by
===========
* audiere
* libtheora
* SDL1_mixer
* SDL2_mixer
